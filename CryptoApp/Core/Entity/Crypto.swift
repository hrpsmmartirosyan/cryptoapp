//
//  Crypto.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation

public struct Crypto: Decodable {
    public let name: String
    public let description: String?
    public let lastPrice: String
    public let lowPrice: String
    public let highPrice: String

    private enum CodingKeys: String, CodingKey {
        case name = "symbol"
        case description = "baseAsset"
        case lastPrice
        case lowPrice
        case highPrice
    }
}

