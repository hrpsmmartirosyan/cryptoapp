//
//  Account.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation

public struct Balance: Decodable {
    public let balance: String
    public let symbol: String
}

public struct Account: Decodable {
    public let email: String
    public let status: String
    public let date: String
    public let balance: Balance

    private enum CodingKeys: String, CodingKey {
        case email
        case status
        case date = "created_at"
        case balance
    }
}
