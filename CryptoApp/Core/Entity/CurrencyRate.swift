//
//  CurrencyRate.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation

public struct CurrencyRate: Decodable {
    public let rate: Double

    private enum CodingKeys: String, CodingKey {
        case rate = "USDSEK"
    }
    
    enum RootKeys: String, CodingKey {
          case quotes = "quotes"
      }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RootKeys.self)

        let userContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .quotes)
        rate = try userContainer.decode(Double.self, forKey: .rate)
    }
}
