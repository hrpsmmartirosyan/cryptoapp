//
//  BarChartView.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import SwiftUI
import Charts

struct BarChart: View {
    let data: [CryptoPrice]

    var body: some View {
        ChartsView(data: data)
    }
}

struct CryptoPrice: Identifiable {
    var id = UUID()
    var mount: String
    var value: Double
    var color: Color
}

struct ChartsView: View {
    let data: [CryptoPrice]

    var body: some View {
        List {
            Chart(data) {
                BarMark(
                    x: .value("Mount", $0.mount),
                    y: .value("Value", $0.value)
                )
                .foregroundStyle($0.color)
            }
            .frame(height: 250)
            .chartForegroundStyleScale([
                "low.price" : Color(.green),
                "high.price": Color(.red),
                "current.price": Color(.blue)
            ])
        }
    }
}
