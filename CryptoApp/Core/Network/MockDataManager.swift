//
//  MockDataManager.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation
import Combine

class MockDataManager: Networking {
    func fetch<T: Decodable, R: Routing>(_ routing: R) -> AnyPublisher<T, Error> {
        let filePath = Bundle.main.path(forResource: routing.jsonPath, ofType: "json")
        
        guard let filePath = filePath else {
            fatalError("Could not create filePath")
        }
        
        let fileUrl = URL(fileURLWithPath: filePath)
        let data = try? Data(contentsOf: fileUrl)
        
        guard let data = data, let object = try? JSONDecoder().decode(T.self, from: data) else {
            fatalError("Could not pars object")
        }
        
        return .init(Result<T, Error>
            .Publisher(object))
    }
}

