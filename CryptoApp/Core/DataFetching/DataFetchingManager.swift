//
//  DataFetchingManager.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation
import Combine

class DataFetchingManager {
    /// Manager to fetch data
    /// Will be injected with constructor injection
    /// Can be used network or mock type
    let manager: Networking

    init(with manager: Networking) {
        self.manager = manager
    }

    func execute<T: Decodable, R: Routing, E: Error>(_ route: R, errorType: E.Type) -> AnyPublisher<T, Error> {
        return manager.fetch(route)
    }
}

