//
//  CryptoAppApp.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-24.
//

import SwiftUI

@main
struct CryptoAppApp: App {
    var body: some Scene {
        WindowGroup {
            TabBarView()
        }
    }
}
