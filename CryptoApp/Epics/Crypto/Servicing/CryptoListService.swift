//
//  CryptoListService.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation
import Combine

final class CryptoListService: DataFetchingManager {
    func call(with parameters: CryptoListParameters) -> AnyPublisher<[Crypto], Error> {
        return self.execute(parameters, errorType: Error.self)
    }
}
