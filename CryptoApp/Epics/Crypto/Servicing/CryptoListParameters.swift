//
//  CryptoListParameters.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation

public struct CryptoListParameters: Routing {
    var method: RequestType {
        .GET
    }
    
    var encoding: ParameterEncoding {
        .json
    }
    
    var routPath: String {
        "tickers/24hr"
    }
    var jsonPath: String? {
        "Cryptos"
    }
}


