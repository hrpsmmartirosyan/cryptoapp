//
//  CurrencyRateParameters.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation

public struct CurrencyRateParameters: Routing {
    var method: RequestType {
        .GET
    }
    
    var encoding: ParameterEncoding {
        .json
    }
    
    var queryParams: [String: String]? {
        ["currencies" : "SEK",
         "source" : "USD",
         "access_key" : "cddf78651d75ea6e2c05fc332ff95cd9",
        ]
    }
    
    var baseURLString: String {
        "http://apilayer.net/api/live"
    }
    
    var jsonPath: String? {
        "CurrencyRate"
    }
}
