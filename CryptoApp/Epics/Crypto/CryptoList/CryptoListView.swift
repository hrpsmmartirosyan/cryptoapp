//
//  CryptoListView.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import SwiftUI

struct CryptoListView: View {
    @ObservedObject var viewModel = CryptoListViewModel()
    @State private var selection = Currency.usd
    
    var body: some View {
        NavigationStack {
            switch viewModel.state {
            case .loading:
                ProgressView()
            case .failed(let error):
                ErrorView(error: error)
            case .loaded(let cryptos):
                List() {
                    Section{
                        VStack {
                            Picker("select.currency", selection: $selection) {
                                ForEach(viewModel.currencyList, id: \.self) {
                                    Text($0.rawValue)
                                }
                            }
                            .pickerStyle(.menu)
                            .onChange(of: selection) { newValue in
                                viewModel.updateList(with: newValue)
                            }
                        }
                    }
                    ForEach(cryptos, id: \.self) { model in
                        NavigationLink(destination: CryptoDetailsView(model: model)) {
                            CryptoView(model: model)
                        }
                    }
                }
            }
        }
        .onAppear {
            viewModel.fetchCryptoList()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        CryptoListView()
    }
}
