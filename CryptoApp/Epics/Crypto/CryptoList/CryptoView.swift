//
//  CryptoView.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import SwiftUI

public struct CryptoPresentationModel: Hashable {
    let name: String
    let description: String?
    let lastPrice: Double
    let lowPrice: Double
    let highPrice: Double
}

struct CryptoView: View {
    private let model: CryptoPresentationModel
    
    init(model: CryptoPresentationModel) {
        self.model = model
    }

    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 15) {
                Text(model.name)
                    .font(.headline)
                    .foregroundColor(Color.black)
                Text(model.description ?? "")
                    .font(.body)
                Text("Current price: " + String(describing: model.lastPrice))
                    .font(.headline)
            }
        }
    }
}
