//
//  CryptoListViewModel.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation
import Combine

enum Currency: String {
    case usd = "USD"
    case sek = "SEK"
}

final class CryptoListViewModel: ObservableObject {
    @Published private(set) var state = State.loading
    @Published private(set) var currencyList: [Currency] = [.usd, .sek]

    enum State {
         case loading
         case failed(Error)
         case loaded([CryptoPresentationModel])
     }
    
    // MARK: Private members
    private let cryptoListService: CryptoListService
    private let currencyRateService: CurrencyRateService
    
    private var cryptoCurrencies: [Crypto]?
    private var cancellable = Set<AnyCancellable>()

    public init(manager: Networking = NetworkManager()) {
        self.cryptoListService = CryptoListService(with: manager)
        self.currencyRateService = CurrencyRateService(with: manager)
    }
    
    //MARK: Public API
    func fetchCryptoList() {
        let parameters = CryptoListParameters()
        state = .loading
        
        cryptoListService
            .call(with: parameters)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { (completion) in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    self.state = .failed(error)
                }
            }, receiveValue: { [weak self] (list) in
                guard let self = self else { return }
                
                self.cryptoCurrencies = list
                let cryptos = list.map { CryptoPresentationModel(
                    name: $0.name,
                    description: $0.description,
                    lastPrice: self.convertedPrice(from: $0.lastPrice),
                    lowPrice: self.convertedPrice(from: $0.lowPrice),
                    highPrice: self.convertedPrice(from: $0.highPrice))
                }
                
                self.state = .loaded(cryptos)
            }).store(in: &cancellable)
    }
    
    func updateList(with currency: Currency?) {
        guard let currency = currency else { return }
        
        state = .loading
        
        switch currency {
        case .usd:
            guard let list = cryptoCurrencies else { return }
            
            let cryptos = list.map { CryptoPresentationModel(
                name: $0.name,
                description: $0.description,
                lastPrice: convertedPrice(from: $0.lastPrice),
                lowPrice: convertedPrice(from: $0.lowPrice),
                highPrice: convertedPrice(from: $0.highPrice))
            }
            state = .loaded(cryptos)
        case .sek:
            fetchRate()
        }
    }
    
    private func fetchRate() {
        let parameters = CurrencyRateParameters()
        
        state = .loading
        currencyRateService
            .call(with: parameters)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { (completion) in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    self.state = .failed(error)
                }
            }, receiveValue: { [weak self] (rateData) in
                guard let self = self, let cryptoCurrencies = self.cryptoCurrencies else { return }
                                
                let cryptos = cryptoCurrencies.map { CryptoPresentationModel(
                    name: $0.name,
                    description: $0.description,
                    lastPrice: self.convertedPrice(from: $0.lastPrice, rate: rateData.rate),
                    lowPrice: self.convertedPrice(from: $0.lowPrice, rate: rateData.rate),
                    highPrice: self.convertedPrice(from: $0.highPrice, rate: rateData.rate))
                }
                
                self.state = .loaded(cryptos)
            }).store(in: &cancellable)
    }
    
    private func convertedPrice(from price: String, rate: Double = 1) -> Double {
        return (Double(price) ?? 0.0) * rate
    }
}



