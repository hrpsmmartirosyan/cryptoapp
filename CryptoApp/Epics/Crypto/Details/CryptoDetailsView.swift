//
//  CryptoDetailsView.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import SwiftUI

struct CryptoDetailsView: View {
    var model: CryptoPresentationModel

    var body: some View {
        VStack() {
            HStack {
                Text(model.description ?? "").bold()
            }
                     
            BarChart(data: [CryptoPrice(mount: "\n \(model.lowPrice)", value: model.lowPrice, color: .green),
                            CryptoPrice(mount: "\n \(model.highPrice)", value: model.highPrice, color: .red),
                            CryptoPrice(mount: " \n \(model.lowPrice)", value: model.lastPrice, color: .blue)])
        }
    }
}

