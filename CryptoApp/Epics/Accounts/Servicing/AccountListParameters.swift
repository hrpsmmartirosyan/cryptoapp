//
//  AccountListParameters.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation

public struct AccountListParameters {
    public init() {}
}

extension AccountListParameters: Routing {
    var method: RequestType {
        .GET
    }
    
    var encoding: ParameterEncoding {
        .json
    }
    
    var jsonPath: String? {
        "Accounts"
    }
}
