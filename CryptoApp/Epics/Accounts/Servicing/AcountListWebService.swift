//
//  AcountListService.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation

import Foundation
import Combine

final class AcountListService: DataFetchingManager {
    func call(with parameters: AccountListParameters) -> AnyPublisher<[Account], Error> {
        return self.execute(parameters, errorType: Error.self)
    }
}
