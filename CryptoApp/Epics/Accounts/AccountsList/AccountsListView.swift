//
//  AccountsListView.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import SwiftUI

struct AccountsListView: View {
    @ObservedObject var viewModel = AccountListViewModel()

    var body: some View {
        NavigationStack {
            switch viewModel.state {
            case .loading:
                ProgressView()
            case .failed(let error):
                ErrorView(error: error)
            case .loaded(let accounts):
                List {
                    ForEach(accounts, id: \.self) { section in
                        Section() {
                            ForEach(section, id: \.self) { model in
                                AccountInfoView(model: model)
                            }
                        }
                    }
                }
            }
        }
        .onAppear {
            viewModel.fetchUsersList()
        }
    }
}

struct AccountsListView_Previews: PreviewProvider {
    static var previews: some View {
        AccountsListView()
    }
}
