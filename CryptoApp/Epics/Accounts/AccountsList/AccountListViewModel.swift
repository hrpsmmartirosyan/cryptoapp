//
//  AccountListViewModel.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation
import Combine

final class AccountListViewModel: ObservableObject {
    @Published private(set) var state = State.loading

    // MARK: Private members
    private let service: AcountListService
    private var cancellable = Set<AnyCancellable>()

    public init(manager: Networking = MockDataManager()) {
        self.service = AcountListService(with: manager)
    }
    
    enum State {
         case loading
         case failed(Error)
         case loaded([[AccountInfoCellPresentationModel]])
     }
    
    //MARK: Mock API
    func fetchUsersList() {
        let parameters = AccountListParameters()
        service
            .call(with: parameters)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { (completion) in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    self.state = .failed(error)
                }
            }, receiveValue: { [weak self] (accounts) in
                let list = accounts.map {
                    return [AccountInfoCellPresentationModel(
                        email: $0.email ,
                        status: $0.status,
                        creationDate: $0.date,
                        balanceValue: $0.balance.balance,
                    balanceSymobol: $0.balance.symbol)] }
                
                self?.state = State.loaded(list)
                
            }).store(in: &cancellable)
    }
}

