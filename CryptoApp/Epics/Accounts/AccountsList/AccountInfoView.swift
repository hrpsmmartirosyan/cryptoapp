//
//  AccountInfoView.swift
//  CryptoApp
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import SwiftUI

public struct AccountInfoCellPresentationModel: Hashable {
    let email: String
    let status: String
    let creationDate: String
    let balanceValue: String
    let balanceSymobol: String
}

struct AccountInfoView: View {
    var model: AccountInfoCellPresentationModel
    
    var body: some View {
        VStack {
            HStack {
                Text("balance.title")
                Spacer()
                Text(model.balanceValue + " " + model.balanceSymobol)
                    .font(.headline)
            }
            Divider()
            
            HStack {
                Text("email.title")
                Spacer()
                Text(model.email)
                    .font(.headline)
            }
            Divider()
            HStack {
                Text("status.title")
                Spacer()
                Text(model.status)
                    .font(.headline)
            }
            Divider()
            HStack {
                Text("created.title")
                Spacer()
                Text(model.creationDate)
                    .font(.headline)
            }
        }
    }
}

