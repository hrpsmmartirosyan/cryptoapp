//
//  AccountListView.swift
//  MVVM With SwiftUI
//
//  Created by Hripsime Martirosyan on 2023-10-21.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import SwiftUI

struct TabBarView: View {

    var body: some View {
        NavigationStack {
            TabView() {
                AccountsListView()
                .tabItem {
                    Image("account")
                    Text("accounts.list.title")
                }
                CryptoListView()
                    .tabItem {
                        Image("crypto")
                        Text("crypto.list.title")
                    }
            }
        }
    }
}
