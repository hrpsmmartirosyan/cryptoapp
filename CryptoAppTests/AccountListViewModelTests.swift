//
//  AccountListViewModelTests.swift
//  CryptoAppTests
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation
@testable import CryptoApp
import XCTest

final class AccountListViewModelTests: XCTestCase {
    var sut: AccountListViewModel!
    
    override func setUp() {
        super.setUp()
        
        sut = AccountListViewModel()
    }

    func testAccountListFetchIsSuccess() throws {
        sut.fetchUsersList()
        
        wait()
        
        switch sut.state {
        case .loaded(let accounts):
            XCTAssertEqual(accounts.count, 2)
            XCTAssertEqual(accounts[0][0].email, "abc@abc.com")
            XCTAssertEqual(accounts[0][0].status, "active")
        default: break
        }
    }
}

