//
//  XCTestCase+Wait.swift
//  CryptoAppTests
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation
import XCTest

extension XCTestCase {
    func wait(milliseconds: Int = 1) {
        let delayExpectation = expectation(description: "Waiting for async task to execute")
        let dispatchTime = DispatchTime.now() + .milliseconds(milliseconds)

        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            delayExpectation.fulfill()
        }
        waitForExpectations(timeout: TimeInterval(milliseconds + 1))
    }
}
