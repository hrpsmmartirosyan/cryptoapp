//
//  CryptoListViewModeltests.swift
//  CryptoAppTests
//
//  Created by Hripsime Martirosyan on 2023-10-23.
//  Copyright © 2023 hripsimem. All rights reserved.
//

import Foundation
@testable import CryptoApp
import XCTest

final class CryptoListViewModeltests: XCTestCase {
    var sut: CryptoListViewModel!
    
    override func setUp() {
        super.setUp()
        
        sut = CryptoListViewModel(manager: MockDataManager())
    }

    func testAccountListFetchIsSuccess() throws {
        sut.fetchCryptoList()
        
        wait()
        
        switch sut.state {
        case .loaded(let list):
            XCTAssertEqual(list.count, 4)
            XCTAssertEqual(list.first?.lastPrice, 3.2)
            XCTAssertEqual(list.first?.lowPrice, 25.1)
            XCTAssertEqual(list.first?.highPrice, 26.0)
        default: break
        }
    }
    
    func testUpdateCurrencyWithUSD() {
        sut.fetchCryptoList()
        sut.updateList(with: .usd)
        
        switch sut.state {
        case .loaded(let list):
            XCTAssertEqual(list.count, 4)
            XCTAssertEqual(list.first?.lastPrice, 3.2)
            XCTAssertEqual(list.first?.lowPrice, 25.1)
            XCTAssertEqual(list.first?.highPrice, 26.0)
        default: break
        }
    }
    
    func testUpdateCurrencyWithSEK() {
        sut.fetchCryptoList()
        sut.updateList(with: .sek)
        
        wait()
        switch sut.state {
        case .loaded(let list):
            XCTAssertEqual(list.count, 4)
            XCTAssertEqual(list.first?.lastPrice, 3.2 * 10.1)
            XCTAssertEqual(list.first?.lowPrice, 25.1 * 10.1)
            XCTAssertEqual(list.first?.highPrice, 26.0 * 10.1)
        default: break
        }
    }
}
