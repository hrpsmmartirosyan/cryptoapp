# CryptoApp

Swift Project "CryptoApp" provides us iOS  sample which is using `MVVM` pattern for making some API call and showing the response on the views.
Project has no any special configuration, everything is developed with the native swift language with min os version iOS 16.
For the reactive part is used `Combine` native framework which help to make networking part realy simple.
In app you can also see usage of Charts which shows simple data

### Networking
* `URLSession` native
* The response is camming from online Json  (https://api.wazirx.com/sapi/v1/tickers/24hr,
    http://apilayer.net/api/live?access_key=4dc29544c04b9d4b56bb3845c1ae4142&source=USD&currencies=SEK)
* App has mock support where you can inject mock data and use local json insted of network call. For accounts screen  you can see implementation of this

### Usage
* Clone the repository or download the zip and run

